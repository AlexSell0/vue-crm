# vue-crm
```
https://www.youtube.com/watch?v=CTLonYohENw&list=PLqKQF2ojwm3njNpksFCi8o-_c-9Vva_W0&index=8
```
```
https://github.com/TylerPottsDev/yt-vuejs-firebase9-auth/tree/master/src
```
```
https://github.com/iamshaunjp/Getting-Started-with-Firebase-9/
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
