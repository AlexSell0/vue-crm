import { initializeApp } from "firebase/app"
import { getAuth } from "firebase/auth"
import { getDatabase } from "firebase/database";


const firebaseConfig = {
    apiKey: "AIzaSyDTwwOgbPWqKAFIWOPI9sChF_N33b73SqQ",
    authDomain: "vue-crm-6c108.firebaseapp.com",
    databaseURL: "https://vue-crm-6c108-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "vue-crm-6c108",
    storageBucket: "vue-crm-6c108.appspot.com",
    messagingSenderId: "526655843127",
    appId: "1:526655843127:web:74d10ec8c88406cdf988b2"
}

const app = initializeApp(firebaseConfig)
const db = getDatabase(app);
const auth = getAuth(app)

export { auth, db }