import { createApp } from 'vue'
import App from './App.vue'

import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'
import router from './router'
import messagePlugins from "@/utils/message.plugins";
import store from "@/store";

const Vue = createApp(App)

Vue.use(store)
Vue.use(messagePlugins)
Vue.use(router)

Vue.mount('#app')










