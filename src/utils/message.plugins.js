/* eslint-disable no-param-reassign */

export default {
    install(app) {
        app.config.globalProperties.$message = (text) => {
            console.log(text)
            window.M.toast({ html: text });
        };

        app.config.globalProperties.$error = (text) => {
            console.log(text)
            window.M.toast({ html: `Error: ${text}` });
        };
    },
};
/* eslint-disable no-param-reassign */