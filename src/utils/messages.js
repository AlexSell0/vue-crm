export default {
    'logout': 'Вы вышли из системы',
    'error_required': 'Это поле обязательно для заполнения',
    'error_email': 'Неверный email'
}