export default function dateFilter(value, format= "date"){
    let date = ''
    let options = {}
    if(format === "datetime"){
        options = {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        };
    }else if(format === "time"){
        options = {
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        };
    }else{
        options = {
            day: "numeric",
            month: "numeric",
            year: "numeric",
        };
    }

    date = new Intl.DateTimeFormat('ru-RU', options).format(value)

    return date
}