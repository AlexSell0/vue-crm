import { ref, onValue} from 'firebase/database';
import { db } from '../firebase'
export default {
    state: {
        info: {}
    },

    mutations: {
        setInfo(state, info){
            state.info = info
        },
        clearInfo(state){
            state.info = {}
        }
    },
    actions: {
        async fetchInfo({dispatch, commit}){
            try {
                const uId = await dispatch('getUid')
                const starCountRef = ref(db, `/users/${uId}/info`)

                await onValue(starCountRef, (snapshot) => {
                    const info = snapshot.val();

                    commit('setInfo', info)
                });
            }catch (e) {
                console.log(e)
            }
        }
    },
    getters: {
        info: s => s.info
    }

}