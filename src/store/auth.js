import { auth, db } from '../firebase'
import { signInWithEmailAndPassword, signOut, createUserWithEmailAndPassword } from 'firebase/auth'
import { ref, set } from 'firebase/database';
import router from "@/router";
export default {
    actions: {
        // eslint-disable-next-line
        async login({state, dispatch, commit}, {email, password}) {
            try {
                await signInWithEmailAndPassword(auth, email, password)
                router.push({name: 'home'})

            } catch (error) {
                commit('setError', error)
                // switch(error.code) {
                //     case 'auth/user-not-found':
                //         alert("User not found")
                //         break
                //     case 'auth/wrong-password':
                //         alert("Wrong password")
                //         break
                //     default:
                //         console.log(error)
                //         alert("Something went wrong")
                // }
                //
                // return
            }
        },

        // eslint-disable-next-line
        async register({state, dispatch, commit}, {email, password, name}){

            try {
                // eslint-disable-next-line
                await createUserWithEmailAndPassword(auth, email, password)
                const uid = await dispatch('getUid')

                set(ref(db, `users/${uid}/info`), {
                    bill: 1000,
                    name

                });

                console.log('test')

                router.push({name: 'home'})
            }catch (error) {
                commit('setError', error)
                // switch(error.code) {
                //     case 'auth/user-not-found':
                //         alert("User not found")
                //         break
                //     case 'auth/wrong-password':
                //         alert("Wrong password")
                //         break
                //     default:
                //         console.log(error)
                //         alert("Something went wrong")
                // }
                // console.log(error)

                return
            }
        },

        getUid(){
            return auth.currentUser ? auth.currentUser.uid : null
        },

        // eslint-disable-next-line
        async logout ({ commit }) {
            await signOut(auth)

            router.push('/login?message=logout')
        },
    }
}
